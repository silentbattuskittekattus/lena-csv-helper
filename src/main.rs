use std::error::Error;

use clap::{arg, Command};
use csv::{ReaderBuilder, StringRecord};
use ndarray::{concatenate, s, Array, Array1, Array2, ArrayView2, Axis};

fn main() {
    let matches = Command::new("Differ")
        .version("1.0")
        .author("Jonathan H. <silentbat@proton.me>")
        .about("Converts a csv file of specific format via local differentiation.")
        .arg(arg!([PATH]).required(true).help("path to use"))
        .arg(arg!([OUTPUT_PATH]).required(true).help("output path"))
        .get_matches();

    let path = matches
        .value_of("PATH")
        .expect("value for \"PATH\" not available");

    let data = open_file(path).expect("unable to open file for reading data");

    // rows and cols of the data array
    let (rows, cols) = (data.len(), data[0].len());

    // iterator for csv data, we get every row and in every row we iterate over the entries
    let it = data
        .iter()
        .map(|row| row.iter())
        .flatten()
        .map(|v| v.to_string());

    // data array, holds everything we have in the csv file
    let mut data: Array2<String> = Array::from_iter(it)
        .into_shape((rows, cols))
        .expect("parsing main data");

    // define some slices so we know later where we should put our calculated results
    let arr_slice = s![2usize.., 1usize..102];
    let arr_slice2 = s![2usize.., 2usize..102];

    // --- MODIFY INNER DATA
    // inner data that we want to modify
    let arr: Array2<f64> = data
        .slice(arr_slice)
        .mapv(|v| v.parse().expect("parsing inner data"));
    let res0: ArrayView2<f64> = arr.slice(s![.., 1usize..]);
    let res1: ArrayView2<f64> = arr.slice(s![.., ..-1]);
    let result = &res0 - &res1;

    // write new values back into String array
    result
        .mapv(|v| /*v.to_string()*/ format!("{:.6}", v))
        .assign_to(&mut data.slice_mut(arr_slice2));

    // --- CALCULATE PEAKS
    // calculation of maxima
    let sizes_um: Array1<f64> = data
        .slice(s![1usize, 1usize..102])
        .mapv(|v| v.parse().expect("parsing sizes"));

    let mut peak_size_in_um = Vec::new();
    for row in result.rows() {
        let max_idx = row
            .iter()
            .enumerate()
            .max_by(|pair1, pair2| pair1.1.partial_cmp(pair2.1).expect("comparison of sizes"))
            .map(|(idx, _value)| idx)
            .expect("still comparing sizes?");
        peak_size_in_um.push(sizes_um[max_idx + 1]);
    }

    let peak_iter = vec![String::from("Peak"), String::new()]
        .into_iter()
        .chain(peak_size_in_um.into_iter().map(|v| format!("{:.6}", v)));
    let peak_data: Array2<String> = Array::from_iter(peak_iter)
        .into_shape((rows, 1))
        .expect("packing peak data into string array");

    // --- WRITE OUTPUT
    // pack all the data together
    let data2: Array2<String> = concatenate(Axis(1), &[data.view(), peak_data.view()])
        .expect("creating big data2 array from data and peak infos");

    write_array(
        &data2,
        matches
            .value_of("OUTPUT_PATH")
            .expect("value for OUTPUTPATH not available"),
    )
    .expect("error when writing data to file");
}

fn write_array(data: &Array2<String>, path: &str) -> Result<(), Box<dyn Error>> {
    let mut wtr = csv::WriterBuilder::new()
        .delimiter(b'\t')
        .quote_style(csv::QuoteStyle::NonNumeric)
        .from_path(path)?;

    for row in data.rows() {
        wtr.write_record(row)?;
    }

    Ok(())
}

fn open_file(path: &str) -> Result<Vec<StringRecord>, Box<dyn Error>> {
    let mut rdr = ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(false)
        .from_path(path)?;

    let data = rdr
        .records()
        .into_iter()
        .map(|d| d.expect("no record available to collect"))
        .collect();

    Ok(data)
}
